#include <stdio.h>
#include <stdlib.h>

#define MAXBUTTONS (8)
// For each button, there may be the character plus a ' ' or '-', 
// thus 2x for the max string size, an extra 1 for a NULL terminator
#define MAXSTRING  (MAXBUTTONS * 2 +1)

#define FAIL (-1)
#define SUCCESS (0)

// Cycles through all permutations of n buttons picking r entries, prints to console.
// returns -1 on failure, otherwise 0
int nPermuteR (
                char *combination, // Complete string of the combination
                char *curWr, // pointer to last character populated
                const char *buttons, // Array of n button characters
                const int n, // number of characters to choose from
                const int r, // number of characters yet to be chosen
                const char prevButton // previous button press, or '\0' for no previous button
               );

// Appends a button character to a prefix along with a connector character,
// returning the new pointer to the end of the string
// Append occurs in place, and caller must guarantee there is enough space remaining in the provided string
char *appendButton(
                   char *prefix, // prefix string
                   const char connector, // connector character between prefix and new button
                   const char button); // button to append

void usage(const char *cmd)
{
  printf("Usage:\n");
  printf("%s N\n", cmd);
}

int main (int argc, char **argv)
{
  if (2 != argc)
  {
    usage(argv[0]);
    exit(-1);
  }
  
  int N = atoi(argv[1]);
  if (N < 1 || N > MAXBUTTONS)
  {
    usage(argv[0]);
    printf("Input %s is out of range [1, %d]\n", argv[1], MAXBUTTONS);
    exit(-1);
  }
  
//  printf ("%s generating combinations for 1..%d\n", argv[0], N);

  // create initial list of characters;
  // NB: array is on the stack, reasonable for small values of MAXBUTTONS, better to malloc if it grows too large
  char buttons[MAXBUTTONS];
  for (char idx = 0; idx < N; idx++)
  {
    buttons[idx] = '1' + idx;
  }

  // create a string to re-use rather than copying many times
  // NB: string is on the stack, reasonable for small values of MAXBUTTONS, better to malloc if it grows too large
  char combination[MAXSTRING] = "";

  // Create combinations of 1..N buttons
  for (int buttonCount = 1; buttonCount <= N; buttonCount++)
  {          
    if (SUCCESS != nPermuteR (combination, combination, buttons, N, buttonCount, '\0'))
    {
      printf("%s: ERROR: Something went wrong...\n", __FUNCTION__);
      exit(-1);
    }
  }
}

// Appends a button character to a prefix along with a connector character, returning the new pointer 
// to the end of the string.  Append occurs in place, caller must guarantee there is enough space remaining 
// in the provided string
char *appendButton(char *prefix, const char connector, const char button)
{
  // Don't try to append to a null pointer
  if (NULL == prefix)
  {
    return NULL;
  }

  // If the button is NULL, bail out
  if ('\0' == button)
  {
    return prefix;
  }

  if ('\0' != connector)
  {
    *prefix++ = connector;
  }
  *prefix++ = button;

  // Add the null terminator at the end of the string
  *prefix = '\0';

  return prefix;
}

// Cycles through all permutations of n buttons picking r entries, prints to console
int nPermuteR (
                char *combination, // Complete string of the combination
                char *curWr, // pointer to last character populated
                const char *buttons, // Array of n button characters
                const int n, // number of buttons to choose from
                const int r, // number of buttons yet to be chosen
                const char prevButton // previous button press, or '\0' for no previous button
               )
{
  if (   NULL == combination
      || NULL == curWr)
  {
    printf ("%s: ERROR : Invalid input %p %p\n", __FUNCTION__, combination, curWr);
    return FAIL;
  }
  if (n < r)
  {
    printf ("%s: ERROR: Cannot choose %d from %d\n", __FUNCTION__, r, n);
    return FAIL;
  }

  // When r reaches zero, we've completed the combination, output it 
  if (0 == r)
  {
    printf("%s\n", combination);
    return SUCCESS;
  }

  // We'll be adding 2 more characters to the combination, make sure there is room
  if (curWr - combination > MAXSTRING - 2)
  {
    printf ("%s: ERROR : Insufficient space in the output string\n", __FUNCTION__);
    return FAIL;
  }

  // choose the next button
  for (int idx = 0; idx < n; idx++)
  {
    // create local array of buttons exluding the current one
    // TODO this isn't incredibly efficient... creating a new array of buttons
    // for each iteration through.  The max input is limited to 8, so this level of
    // innefficiency isn't a big problem.  If the max value were substantially larger
    // a different algorithm would be worth considering
    char localButtons[MAXBUTTONS];
    for (int rd = 0, wr = 0; rd < n; rd++)
    {
      if (rd == idx)
      {
        continue;
      }
      else
      {
        localButtons[wr++] = buttons[rd];
      }
    }

    // Now that a button has been chosen, there are two options to consider
    // Is this a single button press, or a pair button press?
    // Obviously a single button press is always to be expected.  A paired button 
    // press may occur only if the current button is greater than the previous
    // in order to avoid double-counting.  ex '2-3' and '3-2' are the same pair, 
    // and should only be considered once, while '2 3' and '3 2' are distinct
    // single button presses.  '\0' aka NULL terminator is used to signal the 
    // previous button did not exist, or cannot be paired
    if ('\0' != prevButton && prevButton < buttons[idx])
    {
      char *nextWr = appendButton(curWr, '-', buttons[idx]);
      if (SUCCESS != nPermuteR (combination, nextWr, localButtons, n - 1, r - 1, '\0'))
      {
        return FAIL;
      }
    }

    char *nextWr = appendButton(curWr, curWr == combination ? '\0' : ' ', buttons[idx]);
    if(SUCCESS != nPermuteR (combination, nextWr, localButtons, n - 1, r - 1, buttons[idx]))
    {
      return FAIL;
    }
  }

  return SUCCESS;
}
