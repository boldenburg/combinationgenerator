#!/bin/bash

cmd="./comboGenerator 0"
echo $cmd
result=`$cmd`
if [ $? -ne 255 ]; then
  echo "FAIL: N=1 should return an error"
  exit -1;
else
  echo "PASS"
fi

cmd="./comboGenerator a"
echo $cmd
result=`$cmd`
if [ $? -ne 255 ]; then
  echo "FAIL: N=a should return an error"
  exit -1;
else
  echo "PASS"
fi

cmd="./comboGenerator 9"
echo $cmd
result=`$cmd`
if [ $? -ne 255 ]; then
  echo "FAIL: N=a should return an error"
  exit -1;
else
  echo "PASS"
fi

# number of entries for each N, generated via spreadsheet
lineCount=(0 1 5 24 136 935 7671 73360 801704)

mkdir -p testoutput
for i in {1..8}
do
  echo "generate $i"
  ./comboGenerator $i > testoutput/N$i.txt
  if [ $? -ne 0 ]; then
    echo "FAIL"
    exit -1;
  else
    # confirm that no duplicates exist
    cat testoutput/N$i.txt | wc -l > testoutput/N$i.txt.count
    cat testoutput/N$i.txt | sort | uniq | wc -l > testoutput/N$i.txt.uniq.count
    cmp testoutput/N$i.txt.count testoutput/N$i.txt.uniq.count
    if [ $? -ne 0 ]; then
      echo "FAIL : duplicates found"
      exit -1;    
    fi
    echo ${lineCount[i]} | cmp testoutput/N$i.txt.count
    if [ $? -ne 0 ]; then
      echo "FAIL : count incorrect"
      exit -1;    
    fi
    echo "PASS"
  fi
done

