comboGenerator

### What is this repository for? ###

Combo Lock Challenge  

Consider a mechanical combination lock with buttons numbered from 1 to N. A combination consists of a sequence of button presses where no button is repeated. Each press may be either a single button, or a pair of buttons pressed simultaneously. The length of a combination must be at least 1.
Write a program in C / C++ that accepts a command line parameter for N (where N may be from 1 to 8). Your program should generate to STDOUT a list of all possible combinations. The order is not important, but no combination should be repeated.  

Each combination should be output as a single line of text. A single button press is represented by the decimal digit of the button. A simultaneous button press is represented by the two buttons separated by a dash, with the lowest valued button first in the pair. Each button press is separated by a space.  

For example for N = 2 here are the valid combinations:  

1  
2  
1-2  
1 2  
2 1  

Your solution must above all be correct. Beyond that, the emphasis should be on robustness, elegance and maintainability; efficiency and compactness of code are also important, but should not interfere with the previously mentioned attributes. Implementations in C++ should not make use of high-level STL algorithms like std::next_permutation.  

Build Status:
[![Run Status](https://api.shippable.com/projects/573535702a8192902e1fecd7/badge?branch=master)](https://app.shippable.com/projects/573535702a8192902e1fecd7)

Test Status: n/a

### How do I get set up? ###

* Summary of set up

* Dependencies  
libc, gcc  
* How to run tests  
compile with 'gcc comboGenerator -o comboGenerator'  
./run.sh  
Expected line counts for given inputs via https://docs.google.com/spreadsheets/d/1tRnT71LX-3Th6zPB4VzcGjxlrGivPvBaM7kADzLPk-s/edit?usp=sharing

### Who do I talk to? ###

* brad.oldenburg@gmail.com